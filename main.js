function myFunction()
{
    var option = document.getElementById('label').value;
    var riadky = document.getElementsByClassName(option);
    hideAll(0);
    console.log(option);
    
    for (let i = 0; i < riadky.length; i++){riadky[i].parentNode.parentNode.style.display = "table-row";}
    
    if (option == "all"){ hideAll(1); }

    function hideAll(x)
    {
        var riadky = document.querySelectorAll('tr');
        var riadkyPocet = document.querySelectorAll('tr').length;
        
        if (x == 0) {
            for (let i = 0; i < riadkyPocet; i++) 
            {
                if (i == 0) {continue; }
                riadky[i].style.display = "none"; 
            }
        }

        if (x == 1) {
            for (let i = 0; i < riadkyPocet; i++) 
            {
                riadky[i].style.display = "table-row";
            }
        }
    }
}
